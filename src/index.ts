import * as http from "http"
import * as zlib from "zlib"
import request from "request"
import yaml from "js-yaml"

interface HelmIndex {
  entries: { [name: string]: Array<HelmVersion> }
}
interface HelmVersion {
  urls: Array<string>
}

export default (req: http.IncomingMessage, res: http.ServerResponse) => {
  if (req.url === undefined || !req.url.endsWith("/index.yaml")) {
    res.writeHead(404)
    res.end()
    return
  }

  const targetUrl = req.url.replace(/^\/(https?):?\/*/, "$1://")
  console.log("request", targetUrl)
  request(targetUrl, (error, response, body) => {
    if (error) {
      console.error(error.message)
      res.writeHead(500)
      res.end()
      return
    }
    console.log("response", response.statusCode, response.statusMessage)
    if (200 < response.statusCode || response.statusCode > 299) {
      res.writeHead(500)
      res.end()
      return
    }

    const baseUrl = targetUrl.substring(0, targetUrl.length - 10)
    const responseBody = updateUrls(baseUrl, body)
    if (responseBody instanceof Error) {
      console.error(responseBody.message)
      res.writeHead(500)
      res.end()
      return
    }

    const compressed = compress(req.headers["accept-encoding"], responseBody)
    if (compressed === null) {
      res.writeHead(200, {
        vary: "accept-encoding",
        "content-type": "application/x-yaml",
        "content-length": Buffer.byteLength(responseBody).toString()
      })
      res.write(responseBody, "utf8")
    } else {
      res.writeHead(200, {
        vary: "accept-encoding",
        "content-type": "application/x-yaml",
        "content-encoding": compressed.encoding,
        "content-length": compressed.body.byteLength
      })
      res.write(compressed.body)
    }
    res.end()
  })
}

const updateUrls = (baseUrl: string, data: string): Error | string => {
  try {
    const index: HelmIndex = yaml.safeLoad(data)
    Object.values(index.entries).forEach(versions => {
      versions.forEach(version => {
        version.urls.forEach((url, i, urls) => {
          if (url.startsWith("http://") || url.startsWith("https://")) return
          urls[i] = baseUrl + url
        })
      })
    })
    return yaml.safeDump(index)
  } catch (error) {
    return error
  }
}

const compress = (
  encodingHeader: string | Array<string> | undefined,
  data: string
): null | { encoding: string; body: Buffer } => {
  const mergedEncodingHeader = Array.isArray(encodingHeader)
    ? encodingHeader.join(",")
    : encodingHeader
  const encodings =
    typeof mergedEncodingHeader === "string"
      ? mergedEncodingHeader
          .split(",")
          .map(splitBySemi)
          .map(encodingFromHeader)
          .sort(sortEncodings)
          .map(getName)
      : []

  for (const encoding of encodings) {
    if (encoding === "deflate") {
      return { encoding: "deflate", body: zlib.deflateSync(data) }
    }
    if (encoding === "gzip") {
      return { encoding: "gzip", body: zlib.gzipSync(data) }
    }
    if (encoding === "br") {
      return { encoding: "br", body: zlib.brotliCompressSync(data) }
    }
  }
  return null
}

const splitBySemi = (str: string): Array<string> => str.split(";")

const getName = <a>(o: { name: a }): a => o.name

interface Encoding {
  index: number
  qvalue: number
  name: string
}

const encodingFromHeader = (e: Array<string>, i: number): Encoding => ({
  index: i,
  qvalue: typeof e[1] === "string" ? parseFloat(e[1].trim().substring(2)) : 1,
  name: typeof e[0] === "string" ? e[0].trim() : ""
})

const sortEncodings = (a: Encoding, b: Encoding): -1 | 0 | 1 =>
  a.qvalue > b.qvalue
    ? -1
    : a.qvalue < b.qvalue
    ? 1
    : a.index > b.index
    ? 1
    : -1
